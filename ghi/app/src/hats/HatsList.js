import React from 'react'
import { Outlet, Link } from "react-router-dom"

class HatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hats: []
        };
        this.handleDelete = this.handleDelete.bind(this);
    }

    async handleDelete(event) {
        const value = event.target.value;

        const hatUrl = `http://localhost:8090/api/hats/${value}`

        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const deletedHat = await response.json();

        };

        this.setState({hats: this.state.hats.filter(hat => String(hat.id) !== value)});

    }

    async componentDidMount() {
        const url = "http://localhost:8090/api/hats/";

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const hatsList = [];
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const hatDetails = await hatResponse.json();

                        hatsList.push(hatDetails);
                    }
                }

                this.setState({hats: hatsList});
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {

        return (
            <>
            <h1>Hats List</h1>
                <Link to="/hats/new">Create a hat!</Link>
                <table className="table table-striped">
                <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                </tr>
                </thead>
                <tbody>
                {this.state.hats.map(hat => {
                    return (
                    <tr key={hat.href}>
                        <td>{hat.style_name}</td>
                        <td>{hat.fabric}</td>
                        <td>{hat.color}</td>
                        <td>{hat.picture_url}</td>
                        <td>{hat.location.closet_name}</td>
                        <td>
                            <button value={hat.id} onClick={this.handleDelete}>Delete</button>
                        </td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            <Outlet />
      </>
        )
    }
}


export default HatList
