import React from 'react'

class ShoeCreate extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            closetList: [],
            binsList: [],
            closet_name: "",
            bin: "",
            manufacturer: "",
            model_name: "",
            color: "",
            picture_url:"",
        }
        this.handleFormChange=this.handleFormChange.bind(this)
        this.handleFormSubmit=this.handleFormSubmit.bind(this)
    }
    async componentDidMount() {
        const url_bins = "http://localhost:8080/api/bins/vos"
        const response = await fetch(url_bins)
        if (response.ok) {
            const data = await response.json()
            this.setState ({
                binsList: data.bins
            })
            const closetList = data.bins.map((bin)=>
                bin.closet_name
            )
            this.setState({
                closetList:closetList.filter((closet, index)=> closetList.indexOf(closet) === index)
            })

        }
    }
    handleFormChange(event) {
        this.setState({
            [event.target.id] : event.target.value
        })
    }
    async handleFormSubmit(event) {
        event.preventDefault()
        const {closetList, binsList, closet_name, bin,  ...data} = this.state
        const url_shoes = "http://localhost:8080/api/shoes/"
        data["bin"] = Number(bin.match(/[0-9]/g).join(""))

        const fetchParams = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const shoeResponse = await fetch(url_shoes, fetchParams)
        if (shoeResponse.ok) {
            document.querySelector(`.alert-success`).classList.remove(`d-none`)
            setTimeout(()=>{document.querySelector(`.alert-success`).classList.add(`d-none`)}, 2000)
            Object.keys(data).map(
                key => {
                    return (
                        this.setState({[key]:""}),
                        document.querySelector(`#${key}`).value=""
                        )
                    }
            )

        }
    }


    render() {
        return (
    <>
        <form onSubmit={this.handleFormSubmit}>
            <div className="mb-3">
                <label htmlFor="bin" className="form-label">Shoe Bin</label>
                <select onChange={this.handleFormChange} id="bin" className="form-select" aria-label="select bin number" aria-describedby="binSelectHelp" required>
                    <option value="" ></option>
                        {
                            this.state.binsList.map((bin)=>{
                                return (<option
                                key={bin.import_href} value={bin.import_href}>closet: {bin.closet_name} - bin: {bin.bin_number}</option>)
                            })
                        }
                </select>
                <div id="binSelectHelp" className="form-text">Please select a Shoe Bin</div>
            </div>
            <div className="mb-3">
                <label htmlFor="manufacturer" className="form-label">Manufacturer Name</label>
                <input onChange={this.handleFormChange} type="text" className="form-control" id="manufacturer" required/>
            </div>
            <div className="mb-3">
                <label htmlFor="model_name" className="form-label">Model Name</label>
                <input onChange={this.handleFormChange} type="text" className="form-control" id="model_name" required/>
            </div>
            <div className="mb-3">
                <label htmlFor="color" className="form-label">Shoe Colour</label>
                <input onChange={this.handleFormChange} type="text" className="form-control" id="color" required/>
            </div>
            <div className="mb-3">
                <label htmlFor="picture_url" className="form-label">Picture of Shoe</label>
                <input onChange={this.handleFormChange} type="text" className="form-control" id="picture_url"
                aria-describedby="pictureHelp"/>
                <div id="pictureHelp" className="form-text">Please enter an URL</div>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
            <div className="alert alert-success d-none" role="alert">
                A new shoe has been added!
            </div>
        </form>
    </>
        )
    }
}

export default ShoeCreate
