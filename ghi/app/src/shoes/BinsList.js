import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

export default function BinsList() {
    const [binsList, setBinsList] = useState([])

    useEffect(()=>{
        const fetchParse = async() => {
            const url_binsList = `http://localhost:8100/api/bins`
            const response = await fetch(url_binsList)
            const data = await response.json()
            console.log(data.bins)
            setBinsList(data.bins)
          }
          fetchParse()
    }, [] )

  return (
    <>
        <ul className="list-group mt-10">
            { binsList.map((bin)=>{
                const bin_href = bin.href.match(/[0-9]/g).join("")
                const url_shoeList = `/bins/${bin_href}/shoes`
                return (<Link to={url_shoeList} key={bin.href} className="list-group-item">
                        Closet:{bin.closet_name} Bin Number: {bin.bin_number}/{bin.bin_size}
                        </Link>)
            })}
        </ul>
    </>
  )
}
