import React, {useEffect, useState, useRef } from 'react'
import {useParams, Link} from 'react-router-dom'


export default function ShoesList() {
  const { bin_id } = useParams()
  const [shoesList, setShoesList] = useState([])

  useEffect( ()=> {
    const fetchParse = async() => {
      const url_binShoesList = `http://localhost:8080/api/bins/${bin_id}/shoes`
      const response = await fetch(url_binShoesList)
      const data = await response.json()
      setShoesList(data.shoes)

    }
    fetchParse()
  }, [bin_id])

  return (
    <>
      <div className="list-group text-center mt-10">
        { shoesList.map((shoe)=>{
        const shoe_id = shoe.href.match(/[0-9]/g).join("")
        const url_detail = `/shoes/${shoe_id}`
        return (<Link to={url_detail} state={{shoe_id:shoe_id, bin_id: bin_id}} key={shoe.href} className="list-group-item list-group-item-action mb-3" >{shoe.manufacturer} - {shoe.model_name}</Link>)
      })}
      </div>
    </>
  )
}
