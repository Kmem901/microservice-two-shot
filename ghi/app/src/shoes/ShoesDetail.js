import React, {useState, useEffect} from 'react'
import {useLocation, Link, useNavigate} from 'react-router-dom'

export default function ShoesDetail() {
  const [shoeDetail, setShoeDetail] = useState({})
  const [binDetail, setBinDetail] = useState({})
  const [shoeDelete, setShoeDelete] = useState(false)
  const {shoe_id, bin_id} = useLocation().state
  const navigate = useNavigate()

  useEffect( ()=> {
    const fetchParse = async() => {
      const url_shoeDetail = `http://localhost:8080/api/shoes/${shoe_id}`
      const response = await fetch(url_shoeDetail)
      if (response.ok) {
        const data = await response.json()
        const {bin, ...rest} = data
        console.log(bin, rest)
        setBinDetail(bin)
        setShoeDetail(rest)
      }

    }
    fetchParse()
  }, [shoe_id] )

  useEffect(()=>{
    const fetchParse = async() => {
      const url_shoeDelete = `http://localhost:8080/api/shoes/${shoe_id}`
      const fetchParams = {
        method: "DELETE",
      }
      const response = await fetch(url_shoeDelete, fetchParams)
      if (response.ok) {
        setTimeout(()=>{
          navigate(`/bins/${bin_id}/shoes`, {replace:true})
        }, 100)
      }

    }
    if (shoeDelete===true) {fetchParse()}
  }, [shoeDelete, bin_id, shoe_id, navigate])

  return (
    <>
        <div className="card text-center">
          <div className="card-header">
          Closet: {binDetail.closet_name}
          </div>
          <div className="card-body">
            <h5 className="card-title">Bin number: {binDetail.bin_number}</h5>

              <div>Manufacturer: {shoeDetail.manufacturer}</div>
              <div>Model Name: {shoeDetail.model_name}</div>
              <div>Colour: {shoeDetail.color}</div>
              <div>Bin number: {binDetail.bin_number}</div>

            <button onClick={()=> setShoeDelete(true)} type="submit" name="delete_shoe" className="btn btn-primary m-3">Delete Shoe</button>
            <Link to="shoes/new" state={{shoeDetail: shoeDetail}} className="btn btn-primary m-3">Update Shoe Information</Link>
          </div>
          <div className="card-footer text-muted">

          </div>
        </div>
    </>
  )
}
