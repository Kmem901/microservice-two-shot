import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeCreate from './shoes/ShoeCreate'
import ShoesList from './shoes/ShoesList'
import ShoesDetail from './shoes/ShoesDetail'
import HatCreate from './hats/HatsCreate'
import HatList from './hats/HatsList'
import BinsList from './shoes/BinsList'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/bins" element={<BinsList />} />
          <Route path="/bins/:bin_id/shoes" element={<ShoesList />} />
          <Route path="shoes/:shoe_id" element={<ShoesDetail />} />
          <Route path="hats/" element={<HatList />} />
          <Route path="hats/new" element={<HatCreate />} />
          <Route path="/shoes/new" element={<ShoeCreate />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
