from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "bin",
        "manufacturer",
        "model_name",
    ]
    encoders = {"bin": BinVODetailEncoder()}

class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "bin",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
    ]
    encoders = {"bin": BinVODetailEncoder()}

require_http_methods(["GET", "POST"])
def shoes_list(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id == None:
            shoes = Shoe.objects.all()
            print(shoes)
        else:

            shoes = Shoe.objects.filter(bin=bin_vo_id)
            print(shoes)
        return JsonResponse(
            {"shoes": shoes}, encoder=ShoesListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_number = content["bin"]
            href = f"/api/bins/{bin_number}/"
            bin = BinVO.objects.get(import_href=href)
            content["bin"] = bin
            print(content)
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin does not exist"},
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe, encoder=ShoesDetailEncoder, safe=False
        )

require_http_methods(["GET"])
def bin_vo_list(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        return JsonResponse(
            {"bins": bins}, encoder=BinVODetailEncoder
        )


require_http_methods(["GET", "PUT", "DELETE"])
def shoes_detail(request, shoe_id):
    if request.method == "GET":
        shoe = Shoe.objects.get(pk=shoe_id)
        print(shoe)
        return JsonResponse(
            shoe, encoder=ShoesDetailEncoder, safe=False
        )
    elif request.method == "PUT":
        shoe = Shoe.objects.filter(pk=shoe_id)
        content = json.loads(request.body)
        shoe.update(**content)
        return JsonResponse(
            shoe, ShoesDetailEncoder, safe=False
        )
    else:
        count, _ = Shoe.objects.filter(pk=shoe_id).delete()
        return JsonResponse({"deleted": count>0})
