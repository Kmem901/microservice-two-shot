from django.urls import path
from .api_views import shoes_detail, shoes_list, bin_vo_list
urlpatterns = [
    path("shoes/", shoes_list, name="shoes_create" ),
    path("bins/<int:bin_vo_id>/shoes", shoes_list, name="shoes_list"),
    path("shoes/<int:shoe_id>", shoes_detail, name="shoes_detail"),
    path("bins/vos", bin_vo_list, name="binvos_list")
]
