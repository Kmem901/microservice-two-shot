from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()



    def __str__(self):
        return f"{self.bin_number}"
    class Meta:
        ordering = ("closet_name", "bin_number")

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=20)
    model_name = models.CharField(max_length=35)
    color = models.CharField(max_length=20)
    picture_url = models.URLField(null=True, blank=True)
    bin = models.ForeignKey(
        BinVO, related_name="shoes", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.manufacturer} - {self.model_name}"
    def get_api_url(self):
        return f"/api/shoes/{self.pk}"
    class Meta:
        ordering=("bin", "manufacturer", "model_name", "color")
