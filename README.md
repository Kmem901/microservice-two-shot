# Wardrobify

Team:

* Katelyn - Hat microservice
    * Hat model:
        * fabric
        * style_name
        * color
        * picture_url
        * location
    * LocationVO:
        * closet_name
        * section_number
        * shelf_number

* Sophia - Shoes Micro
* shoes/models:
  * Shoe:
    * manufacturer
    * model_name
    * color
    * picture_url
    * bin_number
  * BinVO:
    * closet_name
    * bin_number
    * bin_size

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
